---------------------------
Contenuti CSS:
---------------------------
Dalla console converter, andare su: 
Freddy -> Product Page (Go to Visual Editor) -> CUSTOM CSS

---------------------------
Contenuti Javascript:
---------------------------
Freddy -> Product Page (Go to Visual Editor) -> CUSTOM JAVASCRIPT

---------------------------
Immagini:
---------------------------
i riferimenti alle icone telefono e spedizione gratis sono presenti nel CSS:
.product-view .service-banner-container .service-banner-wrapper ul li .icon.phone {
    background-image: url('https://stage.freddy.com/wordpress/ita_it/wp-content/uploads/sites/8/2018/06/service_phone.jpg');
}
.product-view .service-banner-container .service-banner-wrapper ul li .icon.free-shipping {
    background-image: url('https://stage.freddy.com/wordpress/ita_it/wp-content/uploads/sites/8/2018/06/service_free_shipping.jpg');
}
i riferimenti alle altre immagini invece sono all'interno del Javascript:
https://stage.freddy.com/wordpress/ita_it/wp-content/uploads/sites/8/2018/06/wrup-1.jpg
https://stage.freddy.com/wordpress/ita_it/wp-content/uploads/sites/8/2018/06/wrup_02.jpg
https://stage.freddy.com/wordpress/ita_it/wp-content/uploads/sites/8/2018/06/wrup_04.jpg

---------------------------
Fonts:
---------------------------
il font icoset aggiornato lo trovi nel branch master_fonts_icoset (ho aggiunto le stelline).

---------------------------
Links:
---------------------------
nel Javascript sono presenti due link:
Reso Gratuito: https://www.freddy.com/ita_it/customer-service/refunds/
Spedizione gratis: https://www.freddy.com/ita_it/customer-service/delivery/

---------------------------
Reviews:
---------------------------
Attivabili una volta aggiornato il font.
Le reviews vengono gestite tramite un JSON, per ogni ID prodotto è possibile specificare percentuale di gradimento e numero di reviews.
Il seguente esempio fa riferimento a prodotti on-line.
var parsed = JSON.parse('{"97386" : {"rating": 90, "reviews" : 15}, "101096" : {"rating": 80, "reviews" : 10}}');
